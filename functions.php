<?php

// Add custom Theme Functions here
// Hook in
add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields' );

// Our hooked in function
function custom_override_default_address_fields( $address_fields ) {
     $address_fields['postcode']['required'] = false;
     return $address_fields;
}

// Hook in
add_filter('woocommerce_checkout_fields','custom_override_checkout_fields');

// Our hooked in function
function custom_override_checkout_fields( $fields ) {
	unset($fields['billing']['billing_city']);
	unset($fields['billing']['billing_state']);
	unset($fields['billing']['billing_company']);
	unset($fields['shipping']['shipping_city']);
	unset($fields['shipping']['shipping_state']);
	unset($fields['shipping']['shipping_company']);
	return $fields;
}

// Hook in
function bbloomer_move_checkout_fields_woo_3( $fields ) {
	$fields['company']['priority'] = 25;
	$fields['company']['priority'] = 26;
    return $fields;
}
add_filter( 'woocommerce_default_address_fields', 'bbloomer_move_checkout_fields_woo_3' );

function iconic_email_order_items_args( $args ) {
    $args['show_image'] = true;
    return $args;
}
add_filter( 'woocommerce_email_order_items_args', 'iconic_email_order_items_args', 10, 1 );

//Add WooCommerce registration date Info to Users Page
/*
 * Create a column. And maybe remove some of the default ones
 * @param array $columns Array of all user table columns {column ID} => {column Name} 
 */
function rudr_modify_user_table( $columns ) {
 
	// unset( $columns['posts'] ); // maybe you would like to remove default columns
	$columns['registration_date'] = 'Registration date'; // add new
	return $columns;
}
add_filter( 'manage_users_columns', 'rudr_modify_user_table' );

/*
 * Fill our new column with the registration dates of the users
 * @param string $row_output text/HTML output of a table cell
 * @param string $column_id_attr column ID
 * @param int $user user ID (in fact - table row ID)
 */
function rudr_modify_user_table_row( $row_output, $column_id_attr, $user ) {
 
	$date_format = 'j M, Y H:i';
	switch ( $column_id_attr ) {
		case 'registration_date' :
			return date( $date_format, strtotime( get_the_author_meta( 'registered', $user ) ) );
			break;
		default:
	}
	return $row_output;
}
add_filter( 'manage_users_custom_column', 'rudr_modify_user_table_row', 10, 3 );

/*
 * Make our "Registration date" column sortable
 * @param array $columns Array of all user sortable columns {column ID} => {orderby GET-param} 
 */
function rudr_make_registered_column_sortable( $columns ) {
	return wp_parse_args( array( 'registration_date' => 'registered' ), $columns );
}
add_filter( 'manage_users_sortable_columns', 'rudr_make_registered_column_sortable' );

function child_scripts() {
//    var_dump(get_stylesheet_directory_uri() .'/style.css'); die;
    wp_enqueue_script('custom-child', get_stylesheet_directory_uri() .'/assets/custom.js', [], '1.0.1', 0);
}
add_action('wp_enqueue_scripts', 'child_scripts');

//Add WooCommerce Customer Order Info to Users Page


////////////////////////////////////////////////////////////
// additional customer info
// https://razorfrog.com/woocommerce-user-order-data-columns/
////////////////////////////////////////////////////////////

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	function add_user_details_columns($columns) {
	    $columns['user_orders'] = 'Orders';
	    $columns['user_total_spent'] = 'Total Spent';
	    return $columns;
	}
	
	function show_user_details_column_content($value, $column_name, $user_id) {
	    if ('user_orders' == $column_name)
	        return wc_get_customer_order_count($user_id);
	    else if ('user_total_spent' == $column_name)
	        return wc_price(wc_get_customer_total_spent($user_id));
	    return $value;
	}
	
	function add_order_details_to_user_list() {
	    add_filter('manage_users_columns', 'add_user_details_columns');
	    add_action('manage_users_custom_column', 'show_user_details_column_content', 10, 3);
	}
	
	add_action('admin_init', 'add_order_details_to_user_list');
	
	add_filter( 'manage_users_sortable_columns', 'my_sortable_cake_column' );
	function my_sortable_cake_column( $columns ) {
	    $columns['user_orders'] = '_order_count';
	    $columns['user_total_spent'] = '_money_spent';
	    return $columns;
	}

	add_action('pre_user_query', 'status_column_orderby');

	function status_column_orderby($userquery){
	
		if('_order_count'==$userquery->query_vars['orderby']) {
			global $wpdb;
			$userquery->query_from .= " LEFT OUTER JOIN $wpdb->usermeta AS alias ON ($wpdb->users.ID = alias.user_id) "; //note use of alias
			$userquery->query_where .= " AND alias.meta_key = '_order_count' "; //which meta are we sorting with?
			$userquery->query_orderby = " ORDER BY alias.meta_value + 0 ".($userquery->query_vars["order"] == "ASC" ? "asc " : "desc ");//set sort order
		} else if('_money_spent'==$userquery->query_vars['orderby']) {
			global $wpdb;
			$userquery->query_from .= " LEFT OUTER JOIN $wpdb->usermeta AS alias ON ($wpdb->users.ID = alias.user_id) "; //note use of alias
			$userquery->query_where .= " AND alias.meta_key = '_money_spent' "; //which meta are we sorting with?
			$userquery->query_orderby = " ORDER BY alias.meta_value + 0 ".($userquery->query_vars["order"] == "ASC" ? "asc " : "desc ");//set sort order
		}
	}
}

function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	}
}
add_action('after_setup_theme', 'remove_admin_bar');

function wpsites_remove_author_capabilities() {
	//$author = get_role( 'customer' );
//	if (current_user_can('administrator')) {
//		$meta_val = 'a:4:{s:8:"customer";b:1;s:4:"read";b:1;s:10:"edit_posts";b:1;s:12:"delete_posts";b:1;}';
//
//		$users = get_users(['role' => 'customer', 'meta_key' => 'wp_capabilities', 'number' => 10, 'meta_value' => $meta_val]);
//		foreach ($users as $user) {
//			var_dump($user->ID);
//
//			$caps = get_user_meta($user->ID, "wp_capabilities", 1);
//			unset($caps['edit_posts']);
////			unset($caps['delete_posts']);
//			var_dump($caps);
////			update_user_meta( $user->ID, 'wp_capabilities', $caps );
//		}
//	}
}
add_action( 'init', 'wpsites_remove_author_capabilities' );

/**
 * Remove the generated product schema markup from Product Category and Shop pages. - NEED TO REMOVE AFTER UPDATE
 */
function wc_remove_product_schema_product_archive() {
	remove_action( 'woocommerce_shop_loop', array( WC()->structured_data, 'generate_product_data' ), 10, 0 );
}
add_action( 'woocommerce_init', 'wc_remove_product_schema_product_archive' );

function com_temp_fix_structured_data_review( $markup, $comment ) {
	$product = wc_get_product( $comment->comment_post_ID );
	$markup['itemReviewed']['aggregateRating'] = array(
		'@type'       => 'AggregateRating',
		'ratingValue' => $product->get_average_rating(),
		'reviewCount' => $product->get_review_count(),
	);
	return $markup;
}
add_filter( 'woocommerce_structured_data_review', 'com_temp_fix_structured_data_review', 10, 2  );

/**
 * WooCommerce: Display Required Field Errors Inline Checkout
 */
function fields_in_label_error( $field, $key, $args, $value ) {
    if ( strpos( $field, '</label>' ) !== false && $args['required'] ) {
        $error = '<span class="error" style="display:none">';
        $error .= sprintf( __( '%s is a required field.', 'woocommerce' ), $args['label'] );
        $error .= '</span>';
        $field = substr_replace( $field, $error, strpos( $field, '</label>' ), 0);
    }
    return $field;
}
add_filter( 'woocommerce_form_field', 'fields_in_label_error', 10, 4 );

remove_action('woocommerce_before_shop_loop_item_title', 'add_woo_pro_badges_on_woo_loop_image', 9);
add_action('woocommerce_before_shop_loop_item', 'add_woo_pro_badges_on_woo_loop_image', 9);