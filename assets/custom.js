(function($){
    // have grouped code into a module for my convenience

    // --------- Single product page ---------
    // (function ($) {
        var old_rating_required;

        $(document).ready(function () {
            if ($('body').hasClass('single-product') && typeof wc_single_product_params !== 'undefined') {
                // disable ugly alert if rating is not set
                // validation will be managed on comment form submission
                old_rating_required = wc_single_product_params.review_rating_required;
                wc_single_product_params.review_rating_required = 'no';
            }
        });

        $(document).on('spu.box_open', function (e, spu_id) {
            // focus on popup in any possible way. actually works the last way only
            $('#spu-' + spu_id).find('spu-content').focus();
            $('#spu-' + spu_id).click();
            window.location.hash = '#spu-' + spu_id;
        });

        // $(document).on('submit', '#commentform', false); // does not work
        document.addEventListener('submit', function(e){

            if (e.target.id !== 'commentform' ) {
                return;
            }
            e.preventDefault();
            e.stopPropagation();
            $('.comment-form-error').remove();
            $('.has-error').removeClass('has-error');

            var $form = $(e.target);
            var form = $form[0];

            var is_valid = true;

            // check rating, it is required
            var $rating = $form.find( '#rating' );
            var rating  = $rating.val();

            if ( $rating.length > 0 && ! rating && old_rating_required === 'yes' ) {
                is_valid = false;

                var $rating_error = $('<p id="rating-error" class="comment-form-error">בחר דירוג</p>');
                $rating_error.insertAfter($('.comment-form-rating label'));
                $(document).one('change', $rating, function(){
                    $rating_error.remove();
                })
            }

            // check required fields
            var $fields = $form.find('input, textarea');

            console.log($fields, $form);
            $fields.each(function(k, el){
                var $current_el = $(el);
                if (!$current_el.val()) {
                    is_valid = false;

                    var $current_el_error = $('<p class="comment-form-error">שדה נדרש</p>');
                    $current_el_error.insertBefore($current_el);
                    $current_el.addClass('has-error');
                    $current_el.one('change', function () {
                        $current_el_error.remove();
                        $current_el.removeClass('has-error');
                    });
                }
            });

            // validate email
            var $email_field = $fields.filter('#email');
            var email_is_valid = true;

            if ($email_field.length) {
                email_is_valid = validateEmail($email_field.val());
                if ( !email_is_valid && $email_field.val()) { // if there is no value then another error should be displayed
                    var $email_error = $('<p class="comment-form-error">נא להזין אימייל תקין</p>');
                    $email_error.insertBefore($email_field);
                    $email_field.addClass('has-error');
                    $email_field.one('change', function () {
                        $email_error.remove();
                        $email_field.removeClass('has-error');
                    });
                }
            }

            // submit form if valid
            if (is_valid && email_is_valid) {

                if ($form.hasClass('submitting')) { // prevent multiple submissions
                    return false;
                }
                $form.addClass('submitting');

                // just calling .submit() does not work because of button with id="submit".
                // form.submit returns html element instead of function
                HTMLFormElement.prototype.submit.apply(form, []);
            }
            return false;
        }, true);
    // })($);

    // --------- Profile page ---------
    (function ($) {
        $(document).ready(function(){
            if ( $(document).find('div.woocommerce-address-fields, form.edit-account , form.woocommerce-form').length > 0 ) {
                $(document).on('submit', 'form', function (e) {

                    var cList = e.target.classList;
                    if (
                           !cList.contains('woocommerce-form-register')
                        && !cList.contains('woocommerce-form-login')
                        && !cList.contains('edit-account')
                        && !cList.contains('edit-address')
                    ) {
                        return
                    }
                    e.preventDefault();

                    $('.comment-form-error').remove();
                    $('.has-error').removeClass('has-error');

                    var $form = $(e.target);
                    var form = $form[0];

                    var is_valid = true;

                    // check required fields
                    var $fields = $form.find('.validate-required').find('input, select');
                    $fields.each(function (k, el) {
                        var $current_el = $(el);
                        if (!$current_el.val()) {
                            is_valid = false;

                            var $current_el_error = $('<p class="comment-form-error">שדה נדרש</p>');
                            $current_el_error.insertBefore($current_el);
                            $current_el.addClass('has-error');
                            $current_el.one('change', function () {
                                $current_el_error.remove();
                                $current_el.removeClass('has-error');
                            });
                        }
                    });

                    // validate email
                    var $email_field = $fields.filter('#billing_email, #account_email, #reg_email, #username');
                    var email_is_valid = true;

                    if ($email_field.length) {
                        email_is_valid = validateEmail($email_field.val());
                        if ( !email_is_valid && $email_field.val()) { // if there is no value then another error should be displayed
                            var $email_error = $('<p class="comment-form-error">נא להזין אימייל תקין</p>');
                            $email_error.insertBefore($email_field);
                            $email_field.addClass('has-error');
                            $email_field.one('change', function () {
                                $email_error.remove();
                                $email_field.removeClass('has-error');
                            });
                        }
                    }

                    // validate phone
                    var $phone_field = $fields.filter('#billing_phone');
                    var phone_is_valid = true;
                    if ($phone_field.length) {
                        phone_is_valid = /^[0-9]{1,10}$/.test($phone_field.val());
                        if ( !phone_is_valid && $phone_field.val()) { // if there is no value then another error should be displayed
                            var $phone_error = $('<p class="comment-form-error">אינו מספר טלפון תקין</p>');
                            $phone_error.insertBefore($phone_field);
                            $phone_field.addClass('has-error');
                            $phone_field.one('change', function () {
                                $phone_error.remove();
                                $phone_field.removeClass('has-error');
                            });
                        }
                    }

                    // submit form if valid
                    if (is_valid && email_is_valid && phone_is_valid) {

                        if ($form.hasClass('submitting')) { // prevent multiple submissions
                            return false;
                        }
                        $form.addClass('submitting');

                        // submit function excludes submit inputs from submission but they are necessary for 'process_registration()' and 'process_login()'
                        var submit_input = $form.find(':submit').eq(0);
                        $form.append($('<input type="hidden" name="'+submit_input.prop('name')+'" value="'+submit_input.val()+'"/>'))

                        // just calling .submit() does not work because of button with id="submit".
                        // form.submit returns html element instead of function
                        HTMLFormElement.prototype.submit.apply(form, []);
                    } else {
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                    }
                });
            }
        })
    })($);

    // -------- misc --------
})(jQuery);

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
